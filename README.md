EcoCommonsR

Working repo of the EcoCommons R package
------------------------------------------------------------------------

EcoCommonsR is an R interface to run species distribution modelling 
hosted by EcoCommons, a platform for digital modelling and analysis 
needs on ecological and environmental research, currently in construction. 
It enables users to access the code behind the platform and perform 
alterations as nedded. Note that most of the work is fully adapted for the 
platform to run, making it difficult for users to direct apply in their
personal computers.

If you have any comments, questions or suggestions, please contact us.
contact@ecocommons.org.au


To instal the package, first install and load 'devtools' package
[] install.packages("devtools")
[] library (devtools)

Then install the 'ecocommons' package via devtools and load it
[] ecocommons <- devtools::install_git ("ecocommons-australia/ecocommons-platform/ecocommons",
                                        upgrade = "never") 
[] library(ecocommons)


Please use the EcoCommons_source.R  on the inst folder as the main
script to be followed. Please note that the source_file should change
everytime that you are working on a new algorithm. Example: 
test_constraint_map_ann.json should be used to run the ANN algorithm and
test_constraint_map_rf.json should be used to run RandomForest (rf).

At the moment, testing is being done with the "geographical" algorithms, 
where currently issues are being fixed. The machine learning, profile and 
statitical regression algorithms should be working well.

Please refer to the "source-and-ideas.pdf" for extra information on
the package synthax and formating.

NEXT STEPS:  
- fix issue on saving the individual variable responses on the machine  
  learning algorithms
- fix issue on SDM geoconstrained function for geographical algorithms
